﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class MainPlayerGod : NetworkBehaviour {
    public GameObject strikeSprite;
    public float cooldownStrike;
    public bool isStriking;

    //[SyncVar]
    public GameObject peon;
    public static MainPlayerGod singleton;
    [SyncVar]
    public Vector3 destination;

    public GameObject mapGenerator;
    public GameObject prefabMapGenrator;
    public GameObject prefabPeon;
    public static GameObject player;

    void OnPlayerConnected(NetworkPlayer playerS)
    {
        //player = player.
        //Debug.Log("Player connected from " + player.ipAddress + ":" + player.port);
    }

    void Awake()
    {
        singleton = this;
        mapGenerator = (GameObject)Instantiate(prefabMapGenrator, new Vector3(0, 0, -0.1f), Quaternion.identity);
        //peon = (GameObject)Instantiate(prefabPeon, new Vector3(0, 0, -0.1f), Quaternion.identity);
        //NetworkServer.Spawn(peon);
    }
    // Use this for initialization
    void Start () {
        cooldownStrike = 0.5f;
    }

    public GameObject getPeon()
    {
        return peon;
    }
	
	// Update is called once per frame
    [ServerCallback]
	void Update () {
        //destination = destination + new Vector3(0.1f, 0.1f);
        //peon.transform.position = Vector3.MoveTowards(peon.transform.position, destination, 0.5f);
        //Debug.Log(destination);
        //peon.GetComponent<NetworkTransform>().SetDirtyBit(1);

        if (isStriking)
        {
            cooldownStrike -= 0.1f ;
            if (cooldownStrike <= 0f)
            {
                isStriking = false;
                cooldownStrike = 0.5f;
                strikeSprite.SetActive(false);
            }
        }
    }

    public void strike()
    {
        strikeSprite.SetActive(true);
        isStriking = true;
    }
}
