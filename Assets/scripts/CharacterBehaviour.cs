﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
public class CharacterBehaviour : NetworkBehaviour {

    public GameObject shaman;
    public float speed = 0.5f;
    private Vector3 destination;
    private GameObject carry;
    [SyncVar]
    public bool  isReady;
    // Use this for initialization
    void Awake () {
        isReady = false;
        //shaman = (GameObject)Instantiate(shaman, new Vector3(0, 0, 0), Quaternion.identity);

    }
	
	// Update is called once per frame
	void Update () {
        if (carry != null)
        {
            carry.GetComponent<NetworkTransform>().SetDirtyBit(1);
        }

        GameObject tile = MainPlayer.selectedTile;
        if (Input.GetMouseButtonUp(1))
        {
            destination = tile.transform.position;
        }
        if (Input.GetMouseButtonUp(0) && isReady)
        {
            Debug.Log("GetMouseButtonUp(0) !");
            GameObject usable = tile.GetComponent<TileScript>().content;
            if (carry == null && usable != null && isNextTo(tile.transform.position))
            {
                Debug.Log("yeah un lapin !");
                setCarry(usable);
                tile.GetComponent<TileScript>().setContent(null);
            }
            if (carry != null && usable == null && isNextTo(tile.transform.position))
            {
                Debug.Log("aurevoir petit lapin !");
                tile.GetComponent<TileScript>().setContent(carry);
                setCarry(null);
            }
        }
        if ((shaman.transform.position != destination) && isReady)
        {
            CmdMove();
        }
	}

    //[Command]
    private void CmdMove()
    {
        Debug.Log("toto");
        shaman.transform.position = Vector3.MoveTowards(shaman.transform.position, destination, speed);
        //shaman.GetComponent<NetworkTransform>().SetDirtyBit(1);

    }

    private bool isNextTo(Vector3 tile)
    {
        Vector3 pos = shaman.transform.position;
        bool res = (tile.y - 1 <= pos.y  && pos.y <= tile.y + 1)&&(tile.x - 1 <= pos.x && pos.x <= tile.x + 1);
        return res;
    }

    private void setCarry(GameObject o)
    {
        if (o != null)
        {
            Debug.Log("Carry!!");
            o.transform.parent = shaman.transform;
            o.transform.localPosition = new Vector3(0, 1, 0);
            o.GetComponent<NetworkTransform>().SetDirtyBit(1);
        }
        else
        {
            
        }
        carry = o;
    }
}
