﻿using UnityEngine;
using UnityEngine.Networking;

public class MapGenerator : MonoBehaviour {

	public int x = 21;
	public int y = 21;
    public int maxRabbit = 3;

	public GameObject prefab;
    public GameObject prefab_rabbit;

    // Use this for initialization
    void Start () {
        
	}

    //[Command]
    public void CmdSpawn() {
        GameObject rabbit = (GameObject)Instantiate(prefab_rabbit, new Vector3(0, 0, 0), Quaternion.identity);
        //NetworkServer.Sp 
    }

    public void load(NetworkConnection player)
    {
        int nbRabbit = 0;
        for (int i = -(x / 2); i < x / 2; i++)
        {
            for (int j = -(y / 2); j < y / 2; j++)
            {
                GameObject tile = (GameObject)Instantiate(prefab, new Vector3(i, j, 0), Quaternion.identity);
                if (nbRabbit < maxRabbit)
                {
                    int res = Random.Range(1, 2);
                    Debug.Log(res);
                    if (res == 1)
                    {
                        GameObject rabbit = (GameObject)Instantiate(prefab_rabbit, new Vector3(0, 0, 0), Quaternion.identity);
                        //rabbit.GetComponent<NetworkIdentity>().Ass
                        NetworkServer.SpawnWithClientAuthority(rabbit, player);
                        tile.GetComponent<TileScript>().setContent(rabbit);
                        rabbit.GetComponent<NetworkTransform>().SetDirtyBit(1);


                        nbRabbit++;
                        
                    }
                    
                }
                NetworkServer.Spawn(tile);
                //NetworkServer.Spawn(tile);
                //g.GetComponent<SpriteRenderer>().sprite = 
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
