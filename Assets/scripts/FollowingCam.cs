﻿
using UnityEngine;
using System.Collections;

public class FollowingCam : MonoBehaviour {


    // -------------------------- Configuration --------------------------
   

public float panSpeed = 15.0f;
public float zoomSpeed = 100.0f;

public float mouseZoomMultiplier = 5.0f;

public float minZoomDistance = 20.0f;
public float maxZoomDistance = 50.0f;
public float smoothingFactor = 0.1f;
    public float goToSpeed = 0.1f;

    public bool correctZoomingOutRatio = true;

    public static GameObject objectToFollow;
    public Vector3 cameraTarget;

    // -------------------------- Private Attributes --------------------------
    private float currentCameraDistance;
    private Vector3 lastMousePos;
    private Vector3 lastPanSpeed = Vector3.zero;
    private Vector3 goingToCameraTarget = Vector3.zero;
    private bool doingAutoMovement = false;

    // -------------------------- Public Methods --------------------------
    void Start()
    {
        currentCameraDistance = minZoomDistance + ((maxZoomDistance - minZoomDistance) / 2.0f);
        lastMousePos = Vector3.zero;

    }

    void Update()
    {
        UpdateZooming();
        UpdatePosition();
        UpdateAutoMovement();
        lastMousePos = Input.mousePosition;
    }

    void GoTo(Vector3 position)
    {
        doingAutoMovement = true;
        goingToCameraTarget = position;
        objectToFollow = null;
    }

    void Follow(GameObject gameObjectToFollow)
    {
        objectToFollow = gameObjectToFollow;
    }

    private void UpdateZooming()
    {
        float deltaZoom = 0.0f;

            if (Input.GetKey(KeyCode.F))
            {
                deltaZoom = 1.0f;
            }
            if (Input.GetKey(KeyCode.R))
            {
                deltaZoom = -1.0f;
            }
        
            float scroll = Input.GetAxis("Mouse ScrollWheel");
            deltaZoom -= scroll * mouseZoomMultiplier;

        float zoomedOutRatio = correctZoomingOutRatio ? (currentCameraDistance - minZoomDistance) / (maxZoomDistance - minZoomDistance) : 0.0f;
        currentCameraDistance = Mathf.Max(minZoomDistance, Mathf.Min(maxZoomDistance, currentCameraDistance + deltaZoom * Time.deltaTime * zoomSpeed * (zoomedOutRatio * 2.0f + 1.0f)));
    }

    private void UpdatePosition()
    {
        if (objectToFollow != null)
        {
            cameraTarget = Vector3.Lerp(cameraTarget, objectToFollow.transform.position, goToSpeed);
        }

        transform.position = cameraTarget;
        transform.Translate(Vector3.back * currentCameraDistance);
    }

    private void UpdateAutoMovement()
    {
        if (doingAutoMovement)
        {
            cameraTarget = Vector3.Lerp(cameraTarget, goingToCameraTarget, goToSpeed);
            if (Vector3.Distance(goingToCameraTarget, cameraTarget) < 1.0f) {
                doingAutoMovement = false;
            }
        }
    }
}