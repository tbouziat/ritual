﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
public class StartMenu2 : NetworkBehaviour
{

    public UnityEngine.UI.Text hostNameInput;
    public GameObject prefabCharacterBehaviour;
    public GameObject prefabShaman;
    public GameObject characterBehaviour;
    public GameObject prefabMainPlayerGod;
    public GameObject mainPlayerGod;


    public void loadGod()
    {
        //  StartLocalGame();
        //Application.LoadLevel("main");
        mainPlayerGod = (GameObject)Instantiate(prefabMainPlayerGod, new Vector3(0, 0, -0.1f), Quaternion.identity);
        NetworkServer.Spawn(mainPlayerGod);
        
    }

    public void loadHuman()
    {
        // JoinLocalGame();
        //Application.LoadLevel("playerMain");
        characterBehaviour = (GameObject)Instantiate(prefabCharacterBehaviour, new Vector3(0, 0, -0.1f), Quaternion.identity);
        
        //characterBehaviour.GetComponent<CharacterBehaviour>().shaman = MainPlayerGod.singleton.peon;

    }


}
