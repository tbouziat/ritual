﻿using UnityEngine;
using UnityEngine.Networking;

class MyManager : NetworkManager
{
    public GameObject mapGenerator;
    public GameObject prefabCharacterManager;
    public NetworkConnection player;
    public GameObject prefabSpriteLight;
    public GameObject prefabYouWin;

    public GameObject spriteLight;
    public GameObject youwin;

    public bool isLightningLoaded;

    public bool isGod;

    public static MyManager singleton;

    public override void OnClientConnect(NetworkConnection conn)
    {
        if (!Network.isServer)
        {
            
        }
            
    }

    public override void OnServerConnect(NetworkConnection conn)
    {
        player = conn;

    }

    void Start()
    {
        singleton = this;
    }

    public void loadGod()
    {
        GameObject.Find("Main Camera 2").GetComponent<Camera>().enabled = true;
        GameObject.Find("Main Camera").GetComponent<Camera>().enabled = false;
        GameObject.Find("root_GodButtons").transform.FindChild("GodButtons").gameObject.SetActive(true);
        spriteLight = (GameObject)Instantiate(prefabSpriteLight, new Vector3(0, 0, 0), new Quaternion());
        spriteLight.GetComponent<SpriteRenderer>().enabled = false;
        NetworkServer.Spawn(spriteLight);
        isGod = true;
        //GameObject.Find("Main Camera").AddComponent<Str>
    }

    public void loadTerrain()
    {
        mapGenerator.GetComponent<MapGenerator>().load(player);
    }

    public void loadLightning()
    {
        isLightningLoaded = true;
        //GameObject.Find("LightningButton")
        //GameObject.Find("LightningSprite").GetComponent<SpriteRenderer>.enabled = true;
    }

    public void lightning()
    {
        //isLightningLoaded = true;
        Debug.Log("LIGHTNING");
        spriteLight.GetComponent<Lightning>().localLightning();
        spriteLight.GetComponent<Lightning>().Rpclightning();
    }

    void Update()
    {

        if (Network.isServer && GameObject.FindGameObjectsWithTag("Rabbit").Length >= 4)
        {
            if (GameObject.Find("root_YouWinText(Clone)") == null)
            {
                GameObject go = (GameObject)Instantiate(prefabYouWin, new Vector3(0, -2.34f, -1), new Quaternion());
                go.GetComponent<YouWin>().localWin();
                go.GetComponent<YouWin>().RpcWin();
               
                NetworkServer.Spawn(go);
            }

            //GameObject.Find("root_YouWinText").transform.FindChild("YouWinText").gameObject.SetActive(true);
        }
    }

    public void loadHuman()
    {
        isGod = false;
        GameObject characMag = (GameObject)Instantiate(prefabCharacterManager, new Vector3(0, 0, 0), new Quaternion());
        characMag.GetComponent<CharacterBehaviour>().shaman = GameObject.Find("Peon Noir(Clone)");
        //GameObject.Find("CharacterManager").GetComponent<CharacterBehaviour>().shaman = player;
        FollowingCam.objectToFollow = GameObject.Find("Peon Noir(Clone)");
        characMag.GetComponent<CharacterBehaviour>().isReady = true;
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("Tile"))
        {
            GameObject content = go.GetComponent<TileScript>().content;
            if (content != null)
            {
                content.transform.parent = go.transform;
                content.transform.localPosition = new Vector3(0, 0, 0);
            }
        }
    }
}
