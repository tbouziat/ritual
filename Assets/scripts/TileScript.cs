﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class TileScript : NetworkBehaviour {
    [SyncVar]
    public GameObject content;


    public GameObject prefabCrevasse;
    public GameObject prefabFire;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(1)){
            Debug.Log("click droit");
            MainPlayer.selectedTile = this.gameObject;
        }
        if (Input.GetMouseButtonDown(0)){
            Debug.Log("click gauche");
            MainPlayer.selectedTile = this.gameObject;
            if (MyManager.singleton.isGod)
            {
                if (MyManager.singleton.isLightningLoaded)
                {
                    MyManager.singleton.lightning();
                    GameObject go = (GameObject)Instantiate(prefabCrevasse, new Vector3(0, 0, 0), new Quaternion());
                    go.transform.position = transform.position;
                    NetworkServer.Spawn(go);

                    // crame rabbit
                    if (content.name.Contains("Rabbit"))
                    {
                        GameObject fire = (GameObject)Instantiate(prefabFire, new Vector3(0, 0, 0), new Quaternion());
                        fire.transform.position = transform.position;
                        fire.transform.parent = content.transform;
                        NetworkServer.Spawn(fire);
                    }
                }
            }
        }
    }

    public void setContent(GameObject o)
    {
        if (o != null)
        {
            o.transform.parent = transform;
            o.transform.localPosition = new Vector3(0, 0, 0);
        }
        else
        {
            
        }
        content = o;
    }
}
