﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Lightning : NetworkBehaviour {

   // void lightning

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    [ClientRpc]
    public void Rpclightning()
    {
        //isLightningLoaded = true;
        StartCoroutine("coroutineLight");
    }

    public IEnumerator coroutineLight()
    {
        GetComponent<SpriteRenderer>().enabled = true;
        yield return new WaitForSeconds(0.1f);
        GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(0.1f);
        GetComponent<SpriteRenderer>().enabled = true;
        yield return new WaitForSeconds(0.2f);
        GetComponent<SpriteRenderer>().enabled = false;
    }

    public void localLightning()
    {
        //isLightningLoaded = true;
        StartCoroutine("coroutineLight");
    }
}
