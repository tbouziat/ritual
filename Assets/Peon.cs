﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Peon : NetworkBehaviour
{
    public static GameObject selectedTile;
    public bool isDirty;

	// Use this for initialization
	void Start () {
        isDirty = false;
        transform.position = new Vector3(0, 0, 0);
        GetComponent<NetworkTransform>().SetDirtyBit(1);
    }

    // Update is called once per frame
    void Update()
    {
        if (isDirty)
        {
            CmdMove();
            GetComponent<NetworkTransform>().SetDirtyBit(1);
        }
   }

    [Command]
    private void CmdMove()
    {
        transform.position = Vector3.MoveTowards(transform.position, selectedTile.transform.position, 0.5f);
        GetComponent<NetworkTransform>().SetDirtyBit(1);
        isDirty = transform.position != selectedTile.transform.position;
        Debug.Log("isDirty" + isDirty);
    }
}
