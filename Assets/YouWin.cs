﻿using UnityEngine;
using System.Collections;

public class YouWin : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void localWin()
    {
        GetComponentInChildren<TextMesh>().gameObject.transform.parent = Camera.main.transform;
    }

    [RPC]
    public void RpcWin()
    {
        GameObject.Find("YouWinText").transform.parent = Camera.main.transform;
    }
}
